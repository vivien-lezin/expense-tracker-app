import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:expense_tracker_app/model/expense.dart';
import 'expense.repository.dart';

part 'expense.event.dart';
part 'expense.state.dart';

class ExpenseBloc extends Bloc<ExpenseEvent, ExpenseState> {
  final ExpenseRepository _expenseRepository;

  ExpenseBloc({required ExpenseRepository expenseRepository})
      : _expenseRepository = expenseRepository,
        super(const ExpenseState()) {
    on<ExpenseFetched>(_onExpenseFetched);
  }

  Future<void> _onExpenseFetched(
      ExpenseFetched event, Emitter<ExpenseState> emit) async {}
}
