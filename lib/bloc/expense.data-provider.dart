import 'dart:convert';

import 'package:expense_tracker_app/model/dto/create-expense.dto.dart';
import 'package:expense_tracker_app/model/expense.dart';
import 'package:http/http.dart' as http;

class ExpenseDataProvider {
  final http.Client httpClient;

  final String uri = "https://localhost:3000/";

  ExpenseDataProvider(this.httpClient);

  Future<List<Expense>> fetchExpenses() async {
    final response = await http.get(Uri.parse('$uri/expense'));

    if (response.statusCode == 200) {
      List body = jsonDecode(response.body) as List;
      return body
          .map((dynamic json) => Expense.fromJson(json as Map<String, dynamic>))
          .toList();
    } else {
      throw Exception('Failed to load expense');
    }
  }

  Future<bool> _deleteExpense(int id) async {
    final http.Response response = await http.delete(
      Uri.parse('$uri/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Failed to delete expense.');
    }
  }

  Future<Expense> _createExpense(CreateExpenseDto createExpense) async {
    final response = await http.post(
      Uri.parse('$uri/expense'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'title': createExpense.title,
        'category': createExpense.category.index.toString(),
        'amount': createExpense.amount.toString(),
        'date': createExpense.date.toString(),
      }),
    );

    if (response.statusCode == 201) {
      return Expense.fromJson(
          jsonDecode(response.body) as Map<String, dynamic>);
    } else {
      throw Exception('Failed to create expense.');
    }
  }
}