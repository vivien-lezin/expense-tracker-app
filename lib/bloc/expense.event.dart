part of 'expense.bloc.dart';

abstract class ExpenseEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ExpenseFetched extends ExpenseEvent {}
