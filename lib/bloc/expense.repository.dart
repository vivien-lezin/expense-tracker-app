
import 'package:expense_tracker_app/model/expense.dart';
import 'package:http/http.dart' as http;
import 'expense.data-provider.dart';


class ExpenseRepository {
  final ExpenseDataProvider provider = ExpenseDataProvider(http.Client());

  Future<List<Expense>> fetchExpenses() async{
    return await provider.fetchExpenses();
   }
}
