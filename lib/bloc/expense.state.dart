part of 'expense.bloc.dart';

class ExpenseState extends Equatable {

  final ExpenseStatus status;
  final List<Expense> expenses;

  const ExpenseState({
    this.status = ExpenseStatus.initial,
    this.expenses = const <Expense>[],
  });

  ExpenseState copyWith({
    ExpenseStatus? status,
    List<Expense>? expenses,
  }) {
    return ExpenseState(status: status ?? this.status,
        expenses: expenses ?? this.expenses);
  }

  @override
  List<Object?> get props => [status, expenses];

}

enum ExpenseStatus {
  initial,
  success,
  failure,
}