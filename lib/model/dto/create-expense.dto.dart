import 'package:expense_tracker_app/model/category.enum.dart';

class CreateExpenseDto{
  final String title;
  final double amount;
  final DateTime date;
  final Category category;

  CreateExpenseDto(
      {required this.title,
        required this.amount,
        required this.date,
        required this.category});
}