import 'package:expense_tracker_app/model/category.enum.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final formatter = DateFormat.yMd("fr");

const categoryIcons = {
  Category.food: Icons.lunch_dining,
  Category.travel: Icons.flight_takeoff,
  Category.leisure: Icons.movie,
  Category.work: Icons.work,
};

class Expense {
  final String title;
  final double amount;
  final int id;
  final DateTime date;
  final Category category;

  Expense(
      {required this.category,
      required this.title,
      required this.amount,
      required this.date,
      required this.id});

  factory Expense.fromJson(Map<String, dynamic> json) {
    return switch (json) {
      {
        'id': int id,
        'title': String title,
        'amount': double amount,
        'date': String date,
        'category': int category
      } =>
        Expense(
            amount: amount,
            id: id,
            title: title,
            date: DateTime.parse(date),
            category: Category.values[category]),
      _ => throw const FormatException('Failed to load Expense.'),
    };
  }

  String getFormatedData() {
    return formatter.format(date);
  }
}
