import 'package:expense_tracker_app/model/category.enum.dart';
import 'package:expense_tracker_app/model/expense.dart';

class ExpenseBucket {
  final Category category;
  final List<Expense> expenses;

  const ExpenseBucket({required this.category, required this.expenses});

  ExpenseBucket.forCategory(List<Expense> expenses, this.category): expenses = expenses.where((exp) => exp.category == category).toList();

  double get totalExpenses{
    double sum =0;
    for(Expense expense in expenses) {
      sum += expense.amount;
    }
    return sum;
  }


}
