import 'dart:convert';

import 'package:expense_tracker_app/bloc/expense.bloc.dart';
import 'package:expense_tracker_app/bloc/expense.repository.dart';
import 'package:expense_tracker_app/model/dto/create-expense.dto.dart';
import 'package:expense_tracker_app/widget/chart/chart.widget.dart';
import 'package:expense_tracker_app/widget/expenses-list.widget.dart';
import 'package:expense_tracker_app/widget/new_expense.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../model/expense.dart';

class ExpensesScreen extends StatefulWidget {
  @override
  State<ExpensesScreen> createState() => _ExpensesScreenState();
}

class _ExpensesScreenState extends State<ExpensesScreen> {
  late Future<List<Expense>> futureExpenses;

  final String uri = "https://localhost:3000/";

  void _onAddExpense(CreateExpenseDto expense) {

  }

  void _openAddExpenseOverlay() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (ctx) {
          return NewExpense(
            onExpenseAdd: _onAddExpense,
          );
        });
  }

  void _onRemoveExpense(Expense expense) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: const Duration(seconds: 5),
      content: Text("Expense ${expense.title} deleted!"),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final mainContent = RepositoryProvider(
        create: (context) => ExpenseRepository(),
        child: BlocProvider(
          create: (_) => ExpenseBloc(
              expenseRepository:
                  RepositoryProvider.of<ExpenseRepository>(context))
            ..add(ExpenseFetched()),
          child: ExpensesList(onRemoveExpense: _onRemoveExpense,)
        ));
    final chartContent = RepositoryProvider(
        create: (context) => ExpenseRepository(),
        child: BlocProvider(
            create: (_) => ExpenseBloc(
                expenseRepository:
                RepositoryProvider.of<ExpenseRepository>(context))
              ..add(ExpenseFetched()),
            child: const Chart()
        ));

    return Scaffold(
        appBar: AppBar(
          title: const Text("Flutter Expense Tracker"),
          actions: [
            IconButton(
                onPressed: _openAddExpenseOverlay, icon: const Icon(Icons.add))
          ],
        ),
        body: Column(
          children: [
            chartContent,
            mainContent,
          ],
        ));
  }
}
