import 'package:expense_tracker_app/bloc/expense.bloc.dart';
import 'package:expense_tracker_app/model/category.enum.dart';
import 'package:expense_tracker_app/model/expense.dart';
import 'package:expense_tracker_app/model/expense_bucket.dart';
import 'package:expense_tracker_app/widget/chart/chart_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Chart extends StatefulWidget {
  const Chart({super.key});

  @override
  State<Chart> createState() => _ChartState();
}

class _ChartState extends State<Chart> {
  late List<Expense> expenses;

  List<ExpenseBucket> get buckets {
    return [
      ExpenseBucket.forCategory(expenses, Category.food),
      ExpenseBucket.forCategory(expenses, Category.leisure),
      ExpenseBucket.forCategory(expenses, Category.travel),
      ExpenseBucket.forCategory(expenses, Category.work),
    ];
  }

  double get maxTotalExpense {
    double maxTotalExpense = 0;

    for (final bucket in buckets) {
      if (bucket.totalExpenses > maxTotalExpense) {
        maxTotalExpense = bucket.totalExpenses;
      }
    }

    return maxTotalExpense;
  }

  @override
  Widget build(BuildContext context) {
    final isDarkMode =
        MediaQuery.of(context).platformBrightness == Brightness.dark;

    return BlocBuilder<ExpenseBloc, ExpenseState>(builder: (context, state) {
      switch (state.status) {
        case ExpenseStatus.failure:
          return const Center(
            child: Text("failed to fetch expenses"),
          );
        case ExpenseStatus.success:
          if (state.expenses.isEmpty) {
            return const Text('No expenses yet. Add one');
          } else {
            expenses = state.expenses;
            return ListView.builder(
              itemCount: state.expenses.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.all(16),
                  padding: const EdgeInsets.symmetric(
                    vertical: 16,
                    horizontal: 8,
                  ),
                  width: double.infinity,
                  height: 180,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    gradient: LinearGradient(
                      colors: [
                        Theme.of(context).colorScheme.primary.withOpacity(0.3),
                        Theme.of(context).colorScheme.primary.withOpacity(0.0)
                      ],
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                    ),
                  ),
                  child: Column(
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            for (final bucket
                                in buckets) // alternative to map()
                              ChartBar(
                                fill: bucket.totalExpenses == 0
                                    ? 0
                                    : bucket.totalExpenses / maxTotalExpense,
                                amount: bucket.totalExpenses,
                              )
                          ],
                        ),
                      ),
                      const SizedBox(height: 12),
                      Row(
                        children: buckets
                            .map(
                              (bucket) => Expanded(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 4),
                                  child: Icon(
                                    categoryIcons[bucket.category],
                                    color: isDarkMode
                                        ? Theme.of(context)
                                            .colorScheme
                                            .secondary
                                        : Theme.of(context)
                                            .colorScheme
                                            .primary
                                            .withOpacity(0.7),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      )
                    ],
                  ),
                );
              },
            );
          }
        case ExpenseStatus.initial:
          return const Center(child: CircularProgressIndicator());
      }
    });
  }
}
