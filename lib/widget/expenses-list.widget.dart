import 'package:expense_tracker_app/bloc/expense.bloc.dart';
import 'package:expense_tracker_app/model/expense.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:expense_tracker_app/widget/expense.widget.dart';
import 'package:flutter/material.dart';

class ExpensesList extends StatelessWidget {
  final Function onRemoveExpense;

  const ExpensesList({super.key, required this.onRemoveExpense});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExpenseBloc, ExpenseState>(builder: (context, state) {
      switch (state.status) {
        case ExpenseStatus.failure:
          return const Center(
            child: Text("failed to fetch expenses"),
          );
        case ExpenseStatus.success:
          if (state.expenses.isEmpty) {
            return const Text('No expenses yet. Add one');
          } else {
            return ListView.builder(
              itemCount: state.expenses.length,
              itemBuilder: (context, index) {
                return Dismissible(
                  key: ValueKey(state.expenses[index]),
                  direction: DismissDirection.startToEnd,
                  background: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    alignment: AlignmentDirectional.centerStart,
                    color: Theme.of(context)
                        .colorScheme
                        .errorContainer
                        .withOpacity(0.75),
                    margin: EdgeInsets.symmetric(
                        horizontal:
                            Theme.of(context).cardTheme.margin!.horizontal,
                        vertical:
                            Theme.of(context).cardTheme.margin!.vertical - 2),
                    child: Icon(
                      Icons.remove_circle,
                      color: Colors.white.withOpacity(0.5),
                      size: 48,
                    ),
                  ),
                  onDismissed: (direction) {
                    if (direction == DismissDirection.startToEnd) {
                      onRemoveExpense(state.expenses[index]);
                    }
                  },
                  child: ExpenseItem(state.expenses[index]),
                );
              },
            );
          }
        case ExpenseStatus.initial:
          return const Center(child: CircularProgressIndicator());
      }
    });
  }
}
