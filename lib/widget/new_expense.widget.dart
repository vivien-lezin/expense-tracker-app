import "package:expense_tracker_app/model/category.enum.dart";
import 'package:expense_tracker_app/model/dto/create-expense.dto.dart';
import 'package:expense_tracker_app/model/expense.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewExpense extends StatefulWidget {
  final void Function(CreateExpenseDto expense) onExpenseAdd;

  const NewExpense({super.key, required this.onExpenseAdd});

  @override
  State<NewExpense> createState() => _NewExpenseState();
}

class _NewExpenseState extends State<NewExpense> {
  final _titleController = TextEditingController();
  String? _titleErrorMessage;
  final _amountController = TextEditingController();
  String? _amountErrorMessage;
  DateTime? _selectedDate;
  Category _selectedCategory = Category.leisure;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _onPressedDatePicker() async {
    final now = DateTime.now();
    final firstDate = DateTime(now.year - 1, now.month, now.day);
    final pickDate = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: firstDate,
        lastDate: now);
    setState(() {
      _selectedDate = pickDate;
    });
  }

  void _submitExpenseData() {
    List<String> messages = [];
    if (!messages.isNotEmpty && _selectedDate != null) {
      double? amount = double.tryParse(_amountController.text);
      widget.onExpenseAdd(CreateExpenseDto(
          category: _selectedCategory,
          title: _titleController.text,
          amount: amount!,
          date: _selectedDate!));
      Navigator.pop(context);
    }
  }

  @override
  void dispose() {
    _titleController.dispose();
    _amountController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: _titleController,
              maxLength: 50,
              onChanged: (value) {
                if (_titleController.text.trim().isEmpty) {
                  setState(() {
                    _titleErrorMessage = "The title must be set";
                  });
                } else {
                  setState(() {
                    _titleErrorMessage = null;
                  });
                }
              },
              decoration: InputDecoration(
                  labelText: "Title", errorText: _titleErrorMessage),
            ),
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.euro),
                        labelText: "Amount",
                        errorText: _amountErrorMessage),
                    controller: _amountController,
                    onChanged: (value) {
                      if (value == "") {
                        _amountErrorMessage = "The amount must be set";
                      }
                      double? amount =
                          double.tryParse(_amountController.text.trim());
                      if (amount == null) {
                        setState(() {
                          _amountErrorMessage = "The amount must be a number";
                        });
                      } else {
                        if (amount <= 0) {
                          setState(() {
                            _amountErrorMessage = "The amount must be upper 0";
                          });
                        } else {
                          setState(() {
                            _amountErrorMessage = null;
                          });
                        }
                      }
                    },
                  ),
                ),
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(_selectedDate == null
                        ? 'No date selected yet'
                        : DateFormat.yMd().format(_selectedDate!)),
                    IconButton(
                        onPressed: _onPressedDatePicker,
                        icon: const Icon(Icons.calendar_month))
                  ],
                ))
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                DropdownButton(
                    value: _selectedCategory,
                    items: Category.values.map((category) {
                      String categoryName = category.name.toString();
                      categoryName =
                          categoryName.substring(0, 1).toUpperCase() +
                              categoryName.substring(1);
                      return DropdownMenuItem(
                          value: category, child: Text(categoryName));
                    }).toList(),
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          _selectedCategory = value;
                        });
                      }
                    }),
                const Spacer(),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text("Cancel")),
                ElevatedButton(
                    onPressed: _submitExpenseData,
                    child: const Text("Save Expense"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
